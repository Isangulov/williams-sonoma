package testCase;
import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Search {

	@Test
	public void TestCase2() throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.williams-sonoma.com/");
		assertTrue(driver.getTitle().contains("Cookware, Cooking Utensils, Kitchen Decor & Gourmet Foods | Williams-Sonoma"));
		driver.findElement(By.id("ad_email_btn-close")).click();
		driver.findElement(By.cssSelector("input#search-field")).sendKeys("Pan Fry");
		driver.findElement(By.cssSelector("a#btnSearch")).click();
		Thread.sleep(2000);
		assertTrue(driver.getTitle().contains("Pan Fry | Williams-Sonoma"));
		Actions act = new Actions(driver);
		WebElement we = driver.findElement(By.cssSelector("div#content>div:nth-of-type(2)>ul>li:nth-of-type(1)>div:nth-of-type(1)>a:nth-of-type(1)>span"));
		act.moveToElement(we).build().perform();
		driver.findElement(By.cssSelector("div#content>div:nth-of-type(2)>ul>li:nth-of-type(1)>div:nth-of-type(1)>a:nth-of-type(2)>span")).click();
		Thread.sleep(2000);
		WebElement popUp = driver.findElement(By.id("quicklookOverlay"));
		Assert.assertEquals(popUp, popUp);
	
	}

}
