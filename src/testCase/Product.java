package testCase;
import static org.testng.AssertJUnit.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Product {
	
	@Test
	public void TestCase1() throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.williams-sonoma.com/");
		driver.findElement(By.id("ad_email_btn-close")).click();
		assertTrue(driver.getTitle().contains("Cookware, Cooking Utensils, Kitchen Decor & Gourmet Foods | Williams-Sonoma"));
		Actions act = new Actions(driver);
		WebElement we = driver.findElement(By.cssSelector("nav[id='topnav-container']>ul>li:nth-of-type(1)>a"));
		act.moveToElement(we).build().perform();
		Thread.sleep(2000);
		act.moveToElement(driver.findElement(By.cssSelector("nav[id='topnav-container']>ul>li:nth-of-type(1)>div>div:nth-of-type(1)>ul:nth-of-type(1)>li:nth-of-type(1)>a"))).click().build().perform();
		assertTrue(driver.getTitle().contains("Cookware Sets & Best Cookware Sets | Williams-Sonoma"));
		driver.findElement(By.linkText("Le Creuset Stoneware 4-Piece Gift Set")).click();
		assertTrue(driver.getTitle().contains("Le Creuset Stoneware 4-Piece Gift Set | Williams-Sonoma"));
		driver.findElement(By.cssSelector("section.simple-subset>div>div>fieldset:nth-of-type(1)>button")).click();	
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("a#anchor-btn-checkout")).click();	
		assertTrue(driver.getTitle().contains("Shopping Cart | Williams-Sonoma"));
	}
}
